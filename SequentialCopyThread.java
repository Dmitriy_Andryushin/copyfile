/**
 * В этом классе пользователь узнает время при последовательном копировании файлов
 */
public class SequentialCopyThread extends Thread {
    public static void main(String args[]) throws InterruptedException {

        final Thread fileText = new Thread() {
            @Override
            public void run() {
                Copy.copyFile();
            }
        };
        final Thread fileTextCopy = new Thread() {
            @Override
            public void run() {
                Copy.copyFile();
            }
        };
        final long before = System.currentTimeMillis();
        fileText.start();
        fileText.join();
        fileTextCopy.start();
        fileTextCopy.join();
        final long after = System.currentTimeMillis();
        System.out.printf("Время выполнения последовательного копирования: %d", (after - before) / 1000);
    }
}