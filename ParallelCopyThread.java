/**
 * В этом классе пользователь узнает время при параллельном копировании файлов
 */
public class ParallelCopyThread extends Thread {
    public static void main(String args[]) throws InterruptedException {

        final Thread fileText = new Thread() {
            @Override
            public void run() {
                Copy.copyFile();
            }
        };
        final Thread fileTextCopy = new Thread() {
            @Override
            public void run() {
                Copy.copyFile();
            }
        };
        final long before = System.currentTimeMillis();
        fileText.start();
        fileTextCopy.start();
        fileText.join();
        fileTextCopy.join();
        final long after = System.currentTimeMillis();
        System.out.printf("Время выполнения параллельного копирования: %d", (after - before) / 1000);
    }
}