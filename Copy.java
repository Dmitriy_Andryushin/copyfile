import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

public class Copy {
    final static  String FILE_TO_COPY = "Введите полный путь файла, который нужно скопировать: ";
    final static String PATH_TO_COPY = "Введите полный путь, куда копировать файл: ";
    final static String ERROR = "При копировании файла произошла ошибка";

    public static void copyFile() {
        Scanner scanner = new Scanner(System.in);
        System.out.print(FILE_TO_COPY);
        String fileOne = scanner.nextLine();
        System.out.print(PATH_TO_COPY);
        String fileTwo = scanner.nextLine();
        try (FileInputStream input = new FileInputStream(fileOne);
             FileOutputStream output = new FileOutputStream(fileTwo)) {

            byte text;
            while ((text = (byte)input.read()) != -1) {
                output.write(text);
            }
        } catch (IOException e) {
            System.out.println(ERROR);
        }
    }
}
